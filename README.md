# Product Tagging

## This is the project for tagging products based on their category for e commerce websites. Labels are predicted for the product image provided.

## Using the Product Tagging.ipynb notebook :-
Go to https://colab.research.google.com/ and create a new account.

Open this notebook in Google Colab.

From Edit -> Notebook Settings select Hardware Accelerator. Select GPU or TPU for fast processing.

Create a folder with the name 'ProductTagging' in the root of Google Drive.

Create a folder with name 'dataset' and another with name 'output' in ProductTagging folder.

Now copy the contents from the link -> https://drive.google.com/open?id=1cDrs72XW7j6eBtV-oLgsn7G4r9ZrUtV1 into the 'dataset' folder.

Now run the notebook.

## Final accuracy :-
**Before Augmentation -> 82.46%**

**After Augmentation  -> 94.26%**

## Setup :-
Python -- > Python 3.6.5

OS --> Windows 10 (OS build-->17763.253) (Version-->1809)

GPU --> Nvidia Geforce GTX 1060 (6gb)

CPU --> Intel Core i7-8750 @ 2.20GHz

RAM --> 16gb

## Step 1 -> Data Cleaning and Preparation :-
### The image dataset can be cleaned and prepared from productsDataset.json file by running clean_prep.py
python clean_prep.py

Creates dataset folder in project folder with two subfolders -> train and test.

Images used for training will be downloaded in dataset/train folder.

Images used for testing can be put in dataset/test folder.

Checks for the products from the json file with the correct tag format, eg -> OUTERWEAR_JACKETS.

Checks for the products which contain atleast one image.

Downloads those images in their respective categories in dataset/train.

Discards other images.

## Step 2 -> Data Augmentation :-
### The image dataset can be augmented by running augmentation.py
python augmentation.py

Increases the size of dataset if sufficient amount of data is not present for training.

scikit-image library in python can be used to augment the dataset.

Various operations are performed on images for augmentation.

Some of the operations are :-

| S.No. |   Operation to be performed  |                             Command                                 |
| ------|------------------------------|---------------------------------------------------------------------|
|   1   |      Rotating the images.    |        skimage.transform.rotate(image_array, random_degree)         |
|   2   |  Adding Noise to the images. |            skimage.util.random_noise(image_array)                   |
|   3   |      Flipping the images.    |                       image_array[:, ::-1]                          |
|   4   |     Rescaling the images.    | skimage.transform.rescale(image_array, 1.0/4.0, anti_aliasing=False)|

## Step 3 -> Extracting features from images in training dataset :-
### The features can be extracted from the image dataset by running extract_features.py
python extract_features.py

Creates output folder in project folder.

Labels, Features, Models and Weights of images in our dataset are saved in output folder.

## Step 4 -> Training on the image dataset :-
### The model can be trained by running train.py
python train.py

Pre-trained VGG16 model is used.

All the layers of VGG16 other than top fully connected layer are left intact.

The fully connected layer is retrained on our dataset.

Dataset is split into 70% training data and 30% testing data.

Logistic Regression is used to retrain the last layer.

Could use a neural network to retrain the last layer in the future.

Classifier is saved in output folder.

![Alt text](extract_features.png)

## Step 5 -> Testing on new data :-
### The model can be tested by running test.py
python test.py

Put images to be tested in dataset/test folder.

Saved classifier is loaded.

Image is loaded and resized to 224x224 as input to VGG16 is of this size.

Features are extracted.

Prediction is made on test images put in dataset/test folder.

Predictions are printed on screen and saved in predicted_labels.txt (created automatically) in the project folder as well.

## Links referred :-
https://keras.io/applications/#vgg16

https://stackoverflow.com/questions/988228/convert-a-string-representation-of-a-dictionary-to-a-dictionary

https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html

https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html

https://towardsdatascience.com/logistic-regression-using-python-sklearn-numpy-mnist-handwriting-recognition-matplotlib-a6b31e2b166a

https://www.learnopencv.com/keras-tutorial-transfer-learning-using-pre-trained-models/

https://www.analyticsvidhya.com/blog/2018/07/top-10-pretrained-models-get-started-deep-learning-part-1-computer-vision/

https://pillow.readthedocs.io/en/3.1.x/reference/Image.html

https://codelabs.developers.google.com/codelabs/tensorflow-for-poets/#0